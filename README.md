# serverless-benchmarks
A multi cloud provider continuation of https://github.com/yunspace/lambda-platform-perf-comparison

## House Rules

1. No caching
2. Memory = 1GB
3. ID need to be UUID v4